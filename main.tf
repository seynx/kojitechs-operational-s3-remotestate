


resource "aws_vpc" "first_vpc" {
  cidr_block       = var.cidr_block
  
  tags = {
    Name = "vpc_one"
  }
}

output "vpc_id" {
  value= var.cidr_block
}